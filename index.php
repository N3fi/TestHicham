<?php require_once 'session.php'; ?>

<!DOCTYPE html>
<!--
    Test pour Hicham Guennouni
    Par Luc Rosenblatt
-->
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="stl/color.css" rel="stylesheet" type="text/css"/>
        <link href="stl/typo.css" rel="stylesheet" type="text/css"/>
        <link href="stl/layout.css" rel="stylesheet" type="text/css"/>
        <title>Connexion</title>
    </head>
    <body>
        <form id="identification" method="post" action="check.php">
            <div class="box">
                <label for="username">Username</label><input type="text" value="<?php $usernameField ?>"/>
                <label for="password">Password</label><input type="password"  value="<?php $passwordField ?>"/>
            </div>
            <div class="box centerHW">
                <input type="submit" value="Send Code"/>
            </div>
            <div class="box">
                <label for="code">Code</label><input type="password">
            </div>
            <div class="box centerHW">
                <input type="submit" value="Validate"/>
            </div>
        </form>
    </body>
</html>
